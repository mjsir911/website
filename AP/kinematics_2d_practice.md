# Topic 03: Kinematics in 2 dimensions

## Self assessment

Number: 03

Confidence: 7 / 10

Grade: 3 / 5

Weight: 2.8

## Practice Problems

Grade: 

### Problem 1

a) 
D = v * t &#10003;

H = g * D * t &#10007;

b) D = v * H / g &#10007;

c) doubled since velocity is linear in equation &#10003;

d) four times since acceleration is squared &#10003;

e) F = m * g &#10003;

f) K = m * h * g + m * v ** 2 / 2 &#10003;

### Problem 2

a) O<sub>F</sub> = O<sub>H</sub> = O<sub>E</sub> = O<sub>G</sub> >
O<sub>B</sub> = O<sub>D</sub> = O<sub>A</sub> = O<sub>C</sub>

All that matters is starting height &#10003;

b) O<sub>F</sub> = O<sub>H</sub> > O<sub>B</sub> = O</sub>D</sub> >
O<sub>E</sub> = O<sub>G</sub> > O<sub>A</sub> = O<sub>C</sub>

Both starting height and velocity matter, but height is a squared equation
while velocity is linear &#10003;

### Problem 3
