# AP Review test corrections

10. Answer: B; Explanation: ?
12. Answer: D; Explanation: Force is work over distance, to find total work
    calculate area
13. Answer: D; Explanation: Magnitude is irregardless of mass, therefore is
    equal among both
14. Answer: B; Explanation: Momentum is irregardless of mass, but acceleration
    is not, therefore momentum does not change but acceleration does
15. Answer: D; Explanation: ?
19. Answer: B; Explanation: directly inverse
20. Answer: C; Explanation: Attraction and repulsion is squared
21. Answer: C; Explanation: why parabolic and not circular? I guess parabolic
    because it is being accelerated?
26. Answer: D; Explanation: ?
27. Answer: A; Explanation: ?
31. Answer: B; Explanation: F<sub>g</sub> is greater because the elevator is
    decelerating, but the two boxes are still exerting equal forces on
    eachother
32. Answer: D; Explanation: Because the cable would be supporting less mass?
33. Answer: C; Explanation: Because the forces are horizontal and don't require
    coefficient of friction
34. Answer: A; Explanation: Because the forces are vertical
35. Answer: B; Explanation: ?
