# Topic 01: Kinematics in 1 dimension & Topic 03: Kinematics in 2 dimensions

## Self assessment

Number: 1, 3

Confidence: 9 / 10 , 7 / 10

Grade: 7 / 9, 3 / 5

Weight: 2, 2.8

## Sample AP Problems

Grade: 15 / 24

### Single Answer

1)  D &#10007;

2)  B &#10003;

3)  C &#10003;

4)  A &#10007;

5)  D &#10003;

6)  C &#10007;

7)  D &#10003;

8)  D &#10003;

9)  D &#10003;

10) A &#10007;

11) C &#10007;

12) A &#10007;

13) C &#10003;

14) C &#10003;

15) B &#10003;

16) B &#10003;

17) D &#10003;

18) D &#10003;

19) D &#10003;

20) B &#10003;

21) C &#10003;


### Multiple Answer

22) B & C &#10007;

23) A & D &#10007;

24) B & C &#10007;

