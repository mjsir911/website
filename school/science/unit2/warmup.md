# Warm Up

given this v-t graph;

a) Rank the magnitude of the average acceleration during the four segments shown
     a<sub>CD</sub> > a<sub>AB</sub> > a<sub>DE</sub> > a<sub>BC</sub>

b) For which segment does the object move the greatest distance?
     a<sub>AB</SUB> = 13

c) Sketch a graph of Force vs time.
