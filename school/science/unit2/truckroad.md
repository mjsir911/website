         ____
        [     ]
        [     ]
------------------   |
|                 |  |         
|      2200 kg    |][|   2400 kg             |
|                 |  |                       |
-------------------  -----------------------/
    ()      ()


# Truck can exert a friction force of 18000N max on road

(a) What is the max acceleration truck can give SUV

 m = 4600 kg

           18kN Fa        18kN ƒ
       <-------------*------------>
 F = 18,000N

 F = m * a
 F / m = a
 a = 3.9130434782608696

(b) What force does bumper experience

  m = 2200 kg

  m * a = 8608.695652173914
  Ft = m * a
  F - Ft = -9391.304347826086

