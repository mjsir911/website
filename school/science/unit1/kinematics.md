Lab Debrief
==========

* y = m*x + b
* V = a * t + V<sub>0</sub>
  * a = v<sub>2</sub>-v<sub>1</sub>/t<sub>2</sub>-t<sub>1</sub>


Area under a  velocity-time graph
----

A<sub>box</sub> = b*h
on slope line only things that change is shape, triangle now not box
area between velocity line and 0 is displacement in units of y axis

####Side Note
if moving with a negative velocity, do same stuff but displacement is negative

