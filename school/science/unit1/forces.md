|   Force   | Symbol |          Direction          |      Example      |
|:---------:|:------:|:---------------------------:|:-----------------:|
|  Gravity  |   F    | Between 2 objects with mass |   Earth on book   |
|   Normal  |   N    |       90˚ from surface      |   Table on ramp   |
|  Tension  |   T    |      Along the material     |        Rope       |
|  Friction |   ƒ    |       Opposite motion       | Ball through air  |
