# Projectile Motion

## Other interesting notes

* When an object returns to the initial height, it has the same speed again.
* Complementary angles have the same range
* A 45 degree angle gives a projectile the maximum range(you can prove this)

## Velocity is a vector...

Velocity can be broken up into components, just like any other vector.
