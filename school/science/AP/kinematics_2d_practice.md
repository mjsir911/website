# Topic 03: Kinematics in 2 dimensions

## Self assessment

Number: 03

Confidence: 7 / 10

Grade: 3 / 5

Weight: 2.8

## Practice Problems

Grade: 

### Problem 1

a) 
    D = v * t ✓
    H = g * D * t ✗

b) D = v * H / g ✗

c) doubled since velocity is linear in equation ✓

d) four times since acceleration is squared ✓

e) F = m * g ✓

f) K = m * h * g + m * v ** 2 / 2 ✓

### Problem 2

a) O<sub>F</sub> = O<sub>H</sub> = O<sub>E</sub> = O<sub>G</sub> >
O<sub>B</sub> = O<sub>D</sub> = O<sub>A</sub> = O<sub>C</sub>

    All that matters is starting height ✓

b) O<sub>F</sub> = O<sub>H</sub> > O<sub>B</sub> = O</sub>D</sub> >
O<sub>E</sub> = O<sub>G</sub> > O<sub>A</sub> = O<sub>C</sub>

    Both starting height and velocity matter, but height is a squared equation
    while velocity is linear ✓

### Problem 3
