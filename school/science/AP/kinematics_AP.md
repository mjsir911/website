# Topic 01: Kinematics in 1 dimension & Topic 03: Kinematics in 2 dimensions

## Self assessment

Number: 1, 3

Confidence: 9 / 10 , 7 / 10

Grade: 7 / 9, 3 / 5

Weight: 2, 2.8

## Sample AP Problems

Grade: 15 / 24

### Single Answer

1)  D ✗
2)  B ✓
3)  C ✓
4)  A ✗
5)  D ✓
6)  C ✗
7)  D ✓
8)  D ✓
9)  D ✓
10) A ✗
11) C ✗
12) A ✗
13) C ✓
14) C ✓
15) B ✓
16) B ✓
17) D ✓
18) D ✓
19) D ✓
20) B ✓
21) C ✓

### Multiple Answer

22) B & C ✗
23) A & D ✗
24) B & C ✗
