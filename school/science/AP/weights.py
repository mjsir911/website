#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import operator

__appname__     = ""
__author__      = "Marco Sirabella"
__copyright__   = ""
__credits__     = ["Marco Sirabella"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "Marco Sirabella"
__email__       = "msirabel@gmail.com"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""


grades = 7 / 9, 6.5 / 7, 3 / 5, 4.6 / 7, 3 / 6, 5 / 5, 4 / 6, 2.5 / 4, 2 / 7
confid = 9, 9, 7, 5, 10, 8, 5, 8, 3
#((7, 9, 9), (6.5, 7, 9), (3, 5, 7), (4.6, 7, 5), (3, 6, 10), (5, 5, 8), (4, 6,
    #5), (2.5, 4, 8), (2, 7, 3))

weights = sorted([(i + 1, (1 - grade) * c) for i, (grade, c) in
    enumerate(zip(grades, confid))], key=operator.itemgetter(1), reverse=True)
print(weights)
