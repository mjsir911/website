Wavelength $\lambda$: distance from crest to crest of the wave [meter]

Period $T$: time for a single point to complete one cycle [seconds]
