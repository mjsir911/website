\documentclass[12pt]{article}

\usepackage[paperwidth=7.25in, paperheight=9.5in, margin=0.4in]{geometry}
\usepackage{setspace}
\linespread{0.5}
\usepackage{pgfplots}
\usepackage{pgfplotstable, filecontents}
\usepackage{float}
\usepackage{graphicx}

\setlength{\footskip}{20pt}
\makeatletter
\def\@maketitle{%
  \newpage
  \null
  \begin{tabular}[t]{l@{}}
    \@coauthors
  \end{tabular}
  \hfill
  \begin{tabular}[t]{l@{}}
    \today
  \end{tabular}
  \begin{center}%
    {\LARGE \@title \par}%
    \vskip 1.5em%
  \end{center}%
  \par
  \vskip 1.5em}


\def\coauthors#1{\gdef\@coauthors{#1}}
\def\@coauthors{\@latex@warning@no@line{No \noexpand\coauthors given}}

\DeclareRobustCommand*\textsubscript[1]{%
  \@textsubscript{\selectfont#1}}
\def\@textsubscript#1{%
  {\m@th\ensuremath{_{\mbox{\fontsize\sf@size\z@#1}}}}}

\newcommand{\rpm}{\sbox0{$1$}\sbox2{$\scriptstyle\pm$}
  \raise\dimexpr(\ht0-\ht2)/2\relax\box2 }
\makeatother


\title{Unbalanced Forces Lab}
\author{Marco Sirabella}
\coauthors{
Mary Dean-Taylor\\
Munkhjin Ulziibayar\\
Turbat Battulga\\
}

\begin{document}

\maketitle

\section*{Purpose}
To prove a correlation between force, mass, and acceleration.

\section*{Materials}
  \begin{itemize}
    \item{A PASCO Smart-Cart}
    \item{An atwood system}
      \begin{itemize}
        \item{A pulley}
        \item{A string}
        \item{A mass hanger}
      \end{itemize}
    \item{A smart-cart track}
    \item{small-unit masses}
  \end{itemize}

\section*{Procedures}
  \subsection*{Part One}
  \begin{enumerate}
    \item Set up system to resemble diagram (This is beginning state)
    \item Measure the force of gravity being applied on the masses by the earth in newtons
    \item Start measuring acceleration of smart-cart
    \item Release smart-cart from the 50cm mark
    \item Log maximum acceleration (in m/s\textsuperscript{2}) in relation to force of gravity
    \item Repeat from \#3 two more times for a total of three trials
    \item Move mass from mass hanger to smart-cart or from smart-cart to mass hanger in such a way
      that a new recorded force of gravity is being tested
      \begin{itemize}
        \item It is important to not remove any mass from the system, only change the location of it
        \item This is because in this experiment, we must keep mass constant to prove that force and acceleration are directly correlative.
        \item If mass changes, all three variables in the equation change and correlation becomes harder to prove
      \end{itemize}
    \item Repeat from \#2 for five more times or until comfortable with varied amount of data collected
  \end{enumerate}
  \subsection*{Part Two}
  \begin{enumerate}
    \item Set up system to resemble diagram (This is beginning state)
    \item Measure the mass of the system (grams)
    \item Start measuring acceleration of smart-cart
    \item Release smart-cart from the 50cm mark
    \item Log maximum acceleration (in m/s\textsuperscript{2}) in relation to mass of system
    \item Repeat from \#3 two more times for a total of three trials
    \item Remove or add mass from smart-cart in such a way that a new net mass is being tested
      \begin{itemize}
        \item It is important to not change the mass on the mass hangar, only change mass of smart-cart
        \item This is because in this experiment, we must keep net-force constant to prove that mass and acceleration are inversely correlative
        \item If net-force changes, all three variables in the equation change and correlation becomes harder to prove
      \end{itemize}
    \item Repeat from \#2 for five more times or until comfortable with varied amount of data collected
  \end{enumerate}

\section*{Diagram}
\def\svgwidth{400px}
\input{diagram.pdf_tex}

\section*{Data}
\begin{spacing}{1}
\pgfplotstabletypeset[
  col sep=comma,
  columns={Fg, a1, a2, a3, avg, tmass},
  precision=3,
  fixed,
  every head row/.style={before row=\hline,after row=\hline},
  columns/Fg/.style={
    column name={F\textsubscript{g} (N)},
  },
  columns/a1/.style={
    column name={a\textsubscript{1} (m/s\textsuperscript{2})},
    fixed zerofill,
  },
  columns/a2/.style={
    column name={a\textsubscript{2} (m/s\textsuperscript{2})},
    fixed zerofill,
  },
  columns/a3/.style={
    column name={a\textsubscript{3} (m/s\textsuperscript{2})},
    fixed zerofill,
  },
  columns/avg/.style={
    column name={Average (m/s\textsuperscript{2})},
    fixed zerofill,
  },
  columns/tmass/.style={
    column name={Total Mass (g)},
  },
  every last row/.style={after row=\hline},
  every column/.style={column type=|c|},
]{accelVforce.csv}
\end{spacing}

\section*{Graphs}
  \subsection*{Part One}
  \begin{tikzpicture}
    \begin{axis}[
        title=Effect of Force on Acceleration,
        xlabel={Force (N)},
        ylabel={Acceleration (m/s\textsuperscript{2})},
        legend pos=north west,
        xmin=0,
        xmax=2000,
        ymin=0,
    ]
      \addplot [only marks, mark = *] table [x=Fg, y=avg, col sep=comma]{accelVforce.csv};
      \addlegendentry{Average Data}
      \addplot [thick, red] table [x=Fg, y=avg, col sep=comma,
            y={create col/linear regression={y=avg}}
          ] {accelVforce.csv};
      \addplot [thick, red, domain=0:2000] (x, \pgfplotstableregressiona*x + \pgfplotstableregressionb);
      \addlegendentry{%
        Line of best fit
      }
        
    \end{axis}
  \end{tikzpicture}
    \subsubsection*{Analysis}
    As you can see in this graph, the average data points fall in a linear pattern. This led to the belief that the line of best fit would be just that, a line. The line of best fit's equation would be $a = \pgfmathprintnumber{\pgfplotstableregressiona} \cdot N \pgfmathprintnumber[print sign]{\pgfplotstableregressionb}$, where a is acceleration and N is gravity force in Newtons. Attention should be drawn towards the second half of the equation, $b$ in $y = m \cdot x + b$, and jhow close to zero it is. This origin being so close to (0, 0) means that when there is no force, there is no acceleration. The first half of the equation, $m$, is how much any object with the constant amount of mass would accelerate in relation to the Newtons applied to it. This means that the acceleration, when having the constant amount of mass set in this experiment, will always be the amount of newtons multiplied by that number in the equation, $\pgfmathprintnumber{\pgfplotstableregressiona}$. This is important because it proves that the two are directly proportional, acceleration and force.
    \pagebreak
  \subsection*{Part Two}
  \begin{figure}[h!H]
    \caption{The effect of Mass(kg) on acceleration m/s\textsuperscript{2})}
  \includegraphics[scale=0.3]{group.png}
  \end{figure}
    \subsubsection*{Analysis}
    This graph's line of best fit is the exact opposite of the previous. Instead of showing a direct correlation, meaning a multiplicity of the independent variable outputs the dependent variable, this equation shows an inversely proportional equation. An inversely proportional equation is an equation that has a number divided by the independent variable output to the dependent variable. The nature of this equation means that there is no zero, because a zero would have to have a constant divisor of zero, and that just means all real output numbers are zero. The lack of real zero means that, in the second experiment, if mass were to equal zero, the acceleration would be infinite. This is actually pretty realistic in terms of high-level physics. The slope of this equation, $A$ in the graph provided above, means that the dependent variable, acceleration, is equal to $A$ divided by mass in kilograms.


\section*{Calculations}
  \subsection*{Part One}
  To get$a = \pgfmathprintnumber{\pgfplotstableregressiona} \cdot N \pgfmathprintnumber[print sign]{\pgfplotstableregressionb}$, the points were plugged into a standard ti-84 calculator and linear regression was calculated. The output was $m$ and $b$, in context to $y = m \cdot x + b$
  \subsection*{Part Two}
  To get $a = \frac{0.4705\rpm0.01454}{m}$, as seen in the graph in part two under graphs, the class took all the data points gathered in an incomplete number of trials and plugged them all into logger pro. This software allows for lines of best fits, and so an ideal line was chosen to fit the acquired data. Due to many groups' data being acquired into one graph, there is a high bar of error inside of the graph's line of best fit.

\section*{Conclusion}
\begin{spacing}{1}
The data gathered in the experiment shows that acceleration is directly proportional to net force and inversely proportional to mass. The graphs in the experiment proved that with the line of best fit equations, the direct one being multiplicative while the inverse one was division. For experiment one, force and acceleration started at 0 force resulting in 0 acceleration. As the force went up by one, the resulting acceleration went up by \pgfmathprintnumber{\pgfplotstableregressiona}. This confirms our claim that force and acceleration are directly proportional. The second experiment, on the other hand, shows lines of best fits that are exactly opposite to the previous ones. In the second one, $0.4705\rpm0.01454$ gets divided by the mass of the system in kilograms to output the acceleration with a constant net force being applied. In this experiment there are no roots because that would mean there would be zero acceleration with any amount of force, and that rejects laws of physics. This backs up our claim of having inversely proportional acceleration to mass.
\end{spacing}


\section*{Uncertainty Analysis}
\begin{spacing}{1}
There was very little room for uncertainty in this experiment due to how much of a fundamental rule of physics we are observing. A few ways error could have presented itself is inside the recording software. We used software, "PASCO Capstone", to get data through bluetooth from "smart carts". This data could have been inaccurate or skewed, if not zeroed properly or misconfigurations in software or problems with the hardware on the smart cart side. To solve this chance of error would be to learn about the software and hardware and put precautions in place to try and reduce error as much as possible. Another source of error could have been the modified-atwood system itself, requiring the string to be completely horizontal. If the string was skewed at such a miniscule angle, the data would be slightly skewed aswell. The only way to really prevent this is to have a more rigid system in place that does not move every time a trial is tested, and have a camera to make sure the rope is absolutely horizontal. 
\end{spacing}


\end{document}

