set table "test.pgf-plot.table"; set format "%.5f"
set format "%.7e";; f(x) = (a*x)**2+(b*x); a=2; b=2; fit f(x) 'data.csv' u 1:5 via a,b; plot [x=0:6000], f(x); 
