set table "marble.pgf-plot.table"; set format "%.5f"
set format "%.7e";; f(x) = a * (b ** (c * x)); a=3.2213; b=2.7182818284590452353602874713527; c=2.5412; fit f(x) 'marble.csv' u 1:2 via a, c; plot [x=0:1.3] f(x); set print "parameters.dat"; print a, b; 
