\documentclass[12pt]{article}

\usepackage[paperwidth=7.25in, paperheight=9.5in, margin=0.4in]{geometry}
\usepackage{setspace}
\linespread{0.5}
\usepackage{pgfplots}
\usepackage{pgfplotstable, filecontents}
\usepackage{graphicx}
\DeclareGraphicsExtensions{.png,.pdf}
\usepackage{siunitx}
\usepackage{hyperref}
\usepackage{datetime}

\newcommand{\mydate}{}

%\addbibresource{cite.bib}

\setlength{\footskip}{20pt}
\makeatletter
\def\@maketitle{%
  \newpage
  \null
  \begin{tabular}[t]{l@{}}
    \@coauthors
  \end{tabular}
  \hfill
  \begin{tabular}[t]{l@{}}
    %\today
    %\formatdate{3}{6}{1994}
    January 28, 2017% not the way to do this but whatevs
  \end{tabular}
  \begin{center}%
    {\LARGE \@title \par}%
    \vskip 1.5em%
  \end{center}%
  \par
  \vskip 1.5em}


\def\coauthors#1{\gdef\@coauthors{#1}}
\def\@coauthors{\@latex@warning@no@line{No \noexpand\coauthors given}}

\DeclareRobustCommand*\textsubscript[1]{%
  \@textsubscript{\selectfont#1}}
\def\@textsubscript#1{%
  {\m@th\ensuremath{_{\mbox{\fontsize\sf@size\z@#1}}}}}

\newcommand{\rpm}{\sbox0{$1$}\sbox2{$\scriptstyle\pm$}
  \raise\dimexpr(\ht0-\ht2)/2\relax\box2 }
\makeatother


\title{Moment of Inertia Lab}
\author{Marco Sirabella}
\coauthors{
Isabel Mowerey\\
Tyler Lawson\\
}

\begin{document}

\maketitle

\section*{Purpose}
To find a correlation between mass and moment of inertia.

\section*{Materials}
  \begin{itemize}
    \item{PVC T (see diagram)}
    \item{Low-friction pulley}
    \item{Low-mass rope}
    \item{Variable masses}
  \end{itemize}

\section*{Procedures}
  \begin{enumerate}
    \item Set up system to resemble diagram (This is beginning state)
    \item Set up a standardized ending position to log when the mass reaches
    \item Release a mass with a specific mass from the height of the pulley
    \item Log time it takes for mass to reach the bottom using a common stopwatch to measure
    \item Repeat from \#3 two more times for a total of three trials
    \item Change mass of mass to be something different than previously recorded
    \item Repeat from \#3 for six more times or until comfortable with varied amount of data collected
  \end{enumerate}

\section*{Diagram}
\includegraphics[width=400px]{diagram}
\\
Image by Martha Lietz via \url{lietzphysics.blogspot.com}

\section*{Data}
\begin{spacing}{1}
\pgfplotstabletypeset[
  col sep=comma,
  columns={mass, time, alpha, torque, inertia},
  precision=3,
  fixed,
  every head row/.style={before row=\hline,after row=\hline},
  columns/mass/.style={
    column name={$m$\ (\si{$\kilogram$})},
  },
  columns/time/.style={
    column name={$t$\ (\si{$\second$})},
    fixed zerofill,
  },
  columns/alpha/.style={
    column name={$\alpha$\ (\si[per-mode=fraction]{$\radian\per\second\squared$})},
    fixed zerofill,
  },
  columns/torque/.style={
    column name={$\tau$\ (\si{$\joule$})},
    fixed zerofill,
  },
  columns/inertia/.style={
    column name={$I$\ (\si[per-mode=fraction]{$\kilogram\cdot\meter\squared$})},
    fixed zerofill,
  },
  every last row/.style={after row=\hline},
  every column/.style={column type=|c|}
]{data.csv}
\subsection*{Constants}
  $$ g = \SI{9.8}{$\meter\per\second\squared$} $$
  $$ y = \SI{52.2}{$\centi\meter$} $$
  $$ d = \SI{2.7}{$\centi\meter$} $$


\end{spacing}

\section*{Graphs}
  \begin{tikzpicture}
    \begin{axis}[
        title=Effect of angular acceleration\ (\si{$\radian\per\second\squared$}) on torque\ (\si{$\joule$})\newline,
        xlabel={$\alpha$\ (\si[per-mode=fraction]{$\radian\per\second\squared$})},
        ylabel={$\tau$\ (\si{$\joule$})},
        legend pos=outer north east,
        xmin=0,
        xmax=3,
        ymin=0,
    ]
      \addplot [only marks, mark = *] table [x=alpha, y=torque, col sep=comma]{data.csv};
      \addlegendentry{Average Data}
      \addplot [thick, red, domain=-1:3] table [x=alpha, y=torque, col sep=comma,
            y={create col/linear regression={y=torque}}
          ] {data.csv};
      \addlegendentry{%
        Line of best fit\ m = \pgfplotstableregressiona
      }
        
    \end{axis}
  \end{tikzpicture}
    \subsubsection*{Analysis}
    In the graph, the data points for the average angular acceleration for each individual mass corresponds linearly to torque, or energy put into the system. This confirms what we previously believed to be true, which is that the acceleration of a system is purely dependant on the energy put into that system, and this is just the equivalent for an angular system as opposed to a linear one.\\ Because moment of inertia (I) is calculated using $\frac{\alpha}{\tau}$, or angular acceleration over torque, the slope of the line of best fit can be used from this graph to calculate the average moment of inertia of the system. This is useful because the line of best fit treats outliers differently than just pure averaging. The linear nature of this graph proves that within any one system, no matter what amount of torque exerted on it, the moment of inertia is a constant. 


\section*{Calculations}
  Angular acceleration ($\alpha$) was calculated using a kinematic equation $\alpha = \frac{2\cdot y}{t^2}$, where $y$ is the distance displaced, which is a constant 52.2, and $t$ is the time taken for the mass to reach the constant height.\\
  Torque ($\tau$) was calculated by calculating the gravitational force of the mass using $F = m\cdot g$ and then converting to torque using $\tau = F\cdot r$, where $r$ is the radius of the T machine, calculated by dividing $d$ by 2: $r = \frac{d}{2}$. $m$ is the mass of the current mass, while $g$ is the constant gravity.\\
  Moment of inertia (I) was calculated using the line of best fit on the $\alpha$ vs $\tau$ graph. It could also be done by averaging each trial's $\frac{\tau}{\alpha}$, since the methods to get line of best fit is similar to getting average of all moments of inertia. The slope of the line of best fit is $0.022$, while the average of the mathematical calculations is $0.034$, and would be nearer to the line of best fit's slope if outliers were weighted down.

\section*{Conclusion}
\begin{spacing}{1}
  The data gathered in this experiment supports the hypothesis that moment of inertia is constant within an unchanged system. If the system has a constant mass distribution, the ratio between angular acceleration and torque is constant. This is because $I = \frac{\alpha}{\tau}$. This is similar in the relation between angular physics and linear physics, in that the inertia of a system is based off of the mass. The only difference is that the moment of inertia is dependant on the radius of mass to the center of rotation as well as the mass of the system. This is also similar to all the other relations between angular and linear physics, taking into account distance from center of rotation.
\end{spacing}


\section*{Uncertainty Analysis}
\begin{spacing}{1}
  There was room for some uncertainty in the system. A large component of the error is the human aspect of it, the reaction times from when the mass reaches a constant distance underneath the pulley to when the timer stop's measuring the time. The timer also has only a few places of accuracy, as any more would be arbitrary for a phone stopwatch due to human reaction speeds. Another source of error was assuming the pulley was frictionless. The pulley would have been taking some energy put into the system and converting it to angular acceleration and friction. Though due to the nature of the pulleys, very little energy was lost that way because they are specialized to not absorb energy from a system. The strings were also assumed to have no mass, which is obviously false. They just have so little to not make a significant difference. Slipping of the rope could also have been a factor due to the rope being taped on to the T PCB and tape not being the most resistant adhesive to slippage. Out of all the sources of error, the measuring of the time is probably the most significant one.
\end{spacing}


\end{document}

