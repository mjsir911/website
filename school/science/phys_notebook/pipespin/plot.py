#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#import matplotlib.pyplot as plt
import random


__appname__     = ""
__author__      = "Marco Sirabella"
__copyright__   = ""
__credits__     = ["Marco Sirabella"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "Marco Sirabella"
__email__       = "msirabel@gmail.com"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

#f = lambda x: x ** 5 - 3 * x ** 4 - 8 * x ** 3 + 7 * x ** 2 + 5 * x - 11

#x = [random.random() * 10 - 5 for i in range(int(1e1))]

def frange(x, y, jump):
    """http://stackoverflow.com/questions/7267226/range-for-floats"""
    while x < y:
        yield x
        x += jump

#plt.plot([1,2,3,4], [1,4,9,16], 'ro')

def best_fit(X, Y):

    xbar = sum(X)/len(X)
    ybar = sum(Y)/len(Y)
    n = len(X) # or len(Y)

    numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
    denum = sum([xi**2 for xi in X]) - n * xbar**2

    b = numer / denum
    a = ybar - b * xbar

    print('best fit line:\ny = {:.2f} + {:.2f}x'.format(a, b))

    yfit = [a + b * xi for xi in X]
    return X, yfit

def functionify(fstr):
    return eval('lambda x: ' + fstr)

def plot(xrange, yrange, func, precision=0.1):
    x = list(frange(-xrange, xrange + 1, precision))
    y = [func(v) for v in x]
    #plt.plot(x, y)
    #plt.axis([-xrange, xrange, -yrange, yrange])
    #plt.show()

def neural_mediary(nnet, precision=0.1):
    def wrapper(x):
        return nnet.test([x])[0]
    xrange = max(a for b in nnet._dataset for a in b[0])
    yrange = max(a for b in nnet._dataset for a in b[1])
    plot(xrange * 1, yrange * 1, wrapper, precision)



if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Plot a function within a range')

    parser.add_argument('function', metavar='FUNC', type=functionify,
            help='an integer for the accumulator')
    parser.add_argument('xrange', metavar='XRANGE', type=int,
            help='an integer for the accumulator')
    parser.add_argument('yrange', metavar='YRANGE', type=int,
            help='an integer for the accumulator')
    parser.add_argument('--precision', '-p', metavar='PRECISION', type=float,
            default=0.1)
    args = parser.parse_args()
    plot(args.xrange, args.yrange, args.function, args.precision)
