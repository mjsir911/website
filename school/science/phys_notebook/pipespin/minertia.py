#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#import matplotlib.pyplot as plt
import plot

__appname__     = ""
__author__      = "Marco Sirabella"
__copyright__   = ""
__credits__     = ["Marco Sirabella"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "Marco Sirabella"
__email__       = "msirabel@gmail.com"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

d = 2.7 / 100
r = d / 2
g = 9.8
y = 0.522 # cm

data = {
        0.05: 28.5,
        0.1 : 14.0,
        0.2 : 8.84,
        0.3 : 7.15,
        0.4 : 5.84,
        0.5 : 5.35
        }
"""
data = {0.1: [7.95, 7.45, 7.67],
        0.2: [5.28, 5.28, 5.25],
        0.3: [4.42, 4.3, 4.32],
        0.4: [3.59, 3.63, 3.83],
        0.5: [3.05, 3.22, 3.56],
        } # T-arm
for key in data:
    data[key] = round(sum(data[key]) / len(data[key]), 3)
"""

def kinematic(time):
    a = (2 * y) / time ** 2
    return a / r

def strlist(lst):
    return [str(x) for x in lst]

def prettyprint(names, lists):
    maxnames = max(len(name) for name in names) + 1
    for name, list in zip(names, lists):
        print('{0}{2}is\t{1}'.format(name, '\t'.join(strlist(list)), (maxnames - len(name)) * ' '))


roundval = 6
mass, time = list(zip(*sorted(data.items())))
alpha = [round(kinematic(value), roundval) for value in time]
torque = [round(m * g * r, roundval) for m in mass]
inertia = [round(T / a, roundval) for T, a in zip(torque, alpha)]
headers = ('mass', 'time', 'alpha', 'torque', 'inertia')
data    = ( mass,   time,   alpha,   torque,   inertia )
combined = dict(zip(headers, data))

"""
import csv
with open('data.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, headers)
    writer.writeheader()
    for datum in zip(*data):
        writer.writerow(dict(zip(*(headers,datum)))) # why
    #writer.writerow(dict(zip(*(headers, list(zip(*data))[0])))) # this works
    print(combined)
    """
#prettyprint(headers, data)
print(sum(inertia)/len(inertia))
#plt.scatter(alpha, torque)
#plt.plot(*plot.best_fit(alpha, torque))
#plt.show()
exit()
