# SOL vocabulary
Meter
: Rhythm of poetry or music typically divided into equal parts of time value
Narrator
: The person giving the account of a story from their own experiences
Objective
: A goal or primary purpose or a special form in grammar
Onomatopoeia
: A noise in writing form, eg. whoosh
Paradox
: An idea that conflicts with itself
Parody
: A humorous or satirical imitation of a serious idea
Pastoral
: Having characteristics attributed to rural areas
Personification
: Giving an object or animal human attributes and nature
Protagonist
: The leading character in a story
Proverb
: An ancient short popular saying to express a useful moral
Pun
: The humorous use of a word or phrase with multiple meanings, a play on words
Refrain
: To abstain from an impulse to do something
Rhyme
: To use similar sounding words to compliment each-other
Rhythm
: A pattern or procedure with uniform beat, accent, or length
Sarcastic
: To use sarcasm, to say one thing but mean the opposite with use of tone
Satire
: The use of irony, sarcasm, and ridicule in exposing or denouncing an idea
Simile
: 
Speaker (Point-of-view)
: 
Stanza
: 
Symbol
: 
Theme
: 
Tone
: 
