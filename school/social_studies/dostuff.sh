#!/usr/bin/env bash
%s/^/ /g
%s/\s\d\{1,3\}\s/&/g
%s/^\s//g
%s/^\d\{2\}[ A-Za-z]/0&/g
%s/^\d\{1\}[ A-Za-z]/00&/g
%s/^\(\d\d\d\)\([a-zA-Z]\)/\1 \2/g
%sort
%!uniq
g/^\s*$/d
%s/^\d\{3\}\s/<dt>/g
%s/\s*$/<\/dt>  <dd><\/dd>/g
normal gg>G
normal gg>G
