# Politics of the 1920s

0. Warren G. Harding:
  Elected President in 1920
  Campaign of a "Return to normalcy"
  Opposite of wilson

0. Corruption
  Gave power to friends
  Did not keep power in check

0. Immigration in the 1920s
  emergency immigrant ban

0. Hoover
  Another president

0. Themes
  Post-WW1 isolationaism and backlash
  Hands-off government policies
  Prohibition

0. Prohibition
  Illegal alcohol
  People still made it
  Government completely repealed it
