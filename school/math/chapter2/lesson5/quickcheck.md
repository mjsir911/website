Quick Check
-----------
Find all real zeros of:
$$
P(x) = 2x^3 + x^2 - 11x - 10
$$

a) List the procedure for finding the zeros
  1) Find factors of $a$, $q$ being equal to the set
  2) Find factors of $a<sub>0</sub>$, also knows as a-null, and make $P$ equal the set
  3) Then, divide all numbers in the set of $P$ by all numbers in the set of $q$
  4) This gets a list of all zeros.
  5) To find a list of all real zeros, you have to plug in each zero into the original equation and see if it equals zero

b) Find all real zeros
$$
P = ±[-1, -2, -5, -10]
q = ±[1, 2]

zeros = ±[1, 2, 5, 10, 1/2, 5/2]
