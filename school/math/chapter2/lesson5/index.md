#2.5 Zeros of a Polynomial and Rational Equation

A rational oplynomial is of the turn $R(x)=f(x)/g(x)$

Rational Zero Theorem
----------------------

where g(x) ≠ 0
$$
P(x) = a<sub>n-ℝ</sub> * x<sup>n-ℝ</sup>
$$
- a<sub>n-ℝ</sub> ∈ ℝ
- n ∈ ℤ
- n is the degree polynomial


- a<sub>n</sub> - leading coefficient
- a₀ - constant term


###Example 2

$f(x) = 4x^5 + 12x^4 - x - 3$

$$P / q = (±1, ±3) / (±1, ±2, ±4)$$

f(x) = x^3 + 8x^2 + 11x - 20
