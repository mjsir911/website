#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

__appname__     = ""
__author__      = "Marco Sirabella"
__copyright__   = ""
__credits__     = ["Marco Sirabella"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "Marco Sirabella"
__email__       = "msirabel@gmail.com"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

def do_factor(x):
    """This function takes a
    number and prints the factors"""
    factors = []
    x = abs(x)
    #print("The factors of",x,"are:")
    for i in range(1, x + 1):
        if x % i == 0:
            factors.append(i)
    return factors

def zeros(coefficients):
    P = []
    q = []
    for factor in do_factor(coefficients[-1]):
        q.append(factor)
        q.append(factor * -1)
    for factor in do_factor(coefficients[0]):
        P.append(factor)
        P.append(factor * -1)
    zeros = []
    for P_num in P:
        for q_num in q:
            zeros.append(q_num / P_num)

    func = ""

    for y, x in enumerate(reversed(coefficients)):
        """ This just gets a function for debugging use"""
        func += "{} * x ^ {} + ".format(str(x), str(y))

    realzeros = set()

    for zero in zeros:
        total = 0
        for power, coeff in enumerate(reversed(coefficients)):
            total += coeff * (zero ** power)
            #print("{} * ({} ^ {})".format(str(coeff), str(zero), str(power)))
        print(zero)
        print(total)
        if total == 0:
            realzeros.add(zero)

    print(realzeros)

def main():
    #print(sys.argv[1:])
    l = []
    for x in sys.argv[1:]:
        l.append(int(x))
    zeros(l)

if __name__ == '__main__':
    main()
