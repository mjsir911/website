# Polynomial Functions

## Definition & Characteristics

### Definitions of polynomials
$$
P(x) = a<sub>n-ℝ</sub> * x<sup>n-ℝ</sup>
$$
- a<sub>n-ℝ</sub> ∈ ℝ
- n ∈ ℤ
- n is the degree polynomial


- a<sub>n</sub> - leading constant
- a₀ - constant term


### Characteristics of polynomials
- Smooth graph
- Continuous
- No sharp pointed curves


## Zeros of a polynomial function
Is ƒ is a polynomial then the value of x for which $$ƒ(x) = 0$$ is called the zero.

The real zero of a polynomial is the x intercept

## Multiplicity & x-intercept

Given $$f(x) = (x-r)^n(x-q)^m$$, we say that f(x) has zeros:
- r with multiplicity n
- q with multiplicity m
