# 9 - 14

x -> -3-, f(x) -> -∞
x -> -3+, f(x) -> ∞
x -> 1-,  f(x) -> -∞
x -> 1+,  f(x) -> ∞
x -> ∞,   f(x) -> 0
x -> -∞,   f(x) -> -0


# 22

hole = NaN
asymptote:
x = 3

# 24

asymptote:
x = 0
x = 3

# 26

hole: x = 0
asymptote:
x = 3

#36

hole = -6

#38 - 44
#even nums

##38
y = 0

##40
y = 5

##42
y = NaN

#44
y = -3/5

##Solving analytically
* Find the zeros
  - Factor
  - Set equal to zero
  - Solve

###Solve:
$x^2 - 5x ≤ 36$
$x^2 - 5x -36 ≤ 0$

$(x - 9)(x+4) ≤ 0$

-∞ - -4 | -4 - 9 | 9 - ∞
--------|--------|-----
    14  |   0    | 14


