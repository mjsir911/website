# 2.4.4 The remainder & factor theorem

Let f(x) be a polynomial that is divided by $x-c$. If $q(c)$ is the quotent then
$$
f(x) = q(x)(x-c) + r
$$
If $f(x) = (x-c)q(x)$
then $r=0$
$$
f(c) = 0
f(c) = r
$$




Given $f(x) = x^3 - 7x^2 + 5x - 6$
find and interpret $f(3)$

$$f(3) = -27$$
