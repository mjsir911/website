#Angles
##Definition
     ^
    * B
   /      C
A * ----- * >

A - Vertex

Sides: A>B \& A>C

A>C - Initial Side

A>B - Terminal Side

An angle is a measure of the amount of rotation from the initial side.

Angles can be measured in _degrees_ or _radians_

Angles are measured counter clockwise
- When the angle is measured counter clockwise the measure is positive
- When it is measured clockwise the measure is negative
