# Right Triangle Trigonometry

##1)
Sin A = a / c

Cos A = b / c

Tan A = a / b


## 2) Solving a Right Triangle
- Find all missing angles or missing side lengths

### Example one
a = 6
b = 8
c = 10

Sin A = 3/5 Csc = 5/3

Cos A = 4/5 Sec = 5/4

Tan A = 3/4 Cot = 4/3

Its a three-four-five triangle


###Example Two
a = sqrt(120) = 2sqrt(30)
b = 13
c = 17

Sin A = a / c Csc = c / a

Cos A = b / c Sec = c / b

Tan A = a / b Cot = b / a


Sin A = 2sqrt(30) / 17

Cos A = 13 / 17

Tan A = 2sqrt(30) / 13

Csc A = 17sqrt(30) / 60

Sec A = 17 / 13

Cot A = 13sqrt(30) / 60

## 3) Co-functions
Sin A = a / c

Cos B = a / c


Cos B = Sin A

A & B - Complementary

Sin(Ø) = Cos(90 - Ø)


###Example 3
Find a co-function with the same value as the given expression

sin(72) = cos(18)
csc(pi / 3) = sec(pi / 4)
